import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import {DoBootstrap} from "@angular/core";

import {AppComponent} from './app.component';
import {MainComponent} from './main/main.component';
import {GamesComponent} from './games/games.component';
import {RegisterComponent} from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    GamesComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: "",
        component: MainComponent
      },
      {
        path: "games",
        component: GamesComponent
      },
      {
        path: "register",
        component: RegisterComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
